# wkhtmltopdf

This repository contains the binaries for the popular [wkhtmltopdf](https://wkhtmltopdf.org/) which also contains wkhtmltoimage.

The reason for putting this into a library is to be able to install it via [composer](https://getcomposer.org/). The binary included in this repository can be run as-is without installation. 
